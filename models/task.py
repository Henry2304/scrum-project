from odoo import api, fields, models

class TaskManager(models.Model):
    _name = 'task.manager'
    _description = 'Task Management'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    project_id = fields.Many2one('project.manager', string="Project")
    sprint_id = fields.Many2one('sprint.manager', string="Sprint")
    backlog_id = fields.Many2one('product.backlog', string="Back Log")
    dead_line = fields.Date(string="Deadline")
    assigned_to_id = fields.Many2one('res.users', string="Assigned to")
    description = fields.Html(string="Description")

