from odoo import api, fields, models

class ProductBacklog(models.Model):
    _name = 'product.backlog'
    _description = 'Product backlog'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    project_id = fields.Many2one('project.manager', string="Project")
    phase_id = fields.Many2one('phase.manager', string="Phase")
    author_id = fields.Many2one('res.users', string="Author")
    sequence = fields.Integer(string="Sequence")
    wants = fields.Text(string="Wants")
    details = fields.Text(string="Details")


