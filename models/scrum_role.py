from odoo import api, fields, models

class ScrumRole(models.Model):
    _name = 'scrum.role'
    _description = 'Scrum Role'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    description = fields.Text(string="Description")


