from odoo import api, fields, models

class ProjectManager(models.Model):
    _name = 'project.manager'
    _description = 'Project Management'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    project_manager = fields.Many2one('res.users', string="Project Manager")
    members_ids = fields.One2many('members.info', 'scrum_role_id', string="Members")
    start_date = fields.Date(string="Start Date")


class MembersInfo(models.Model):
    _name = 'members.info'
    _description = 'Members Information'

    name = fields.Char(string="Member")
    scrum_role_id = fields.Many2one('scrum.role', string="Scrum Role")
    description = fields.Text(string="Description")







