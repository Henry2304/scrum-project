from odoo import api, fields, models

class PhaseManager(models.Model):
    _name = 'phase.manager'
    _description = 'Phase Management'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    project_id = fields.Many2one('project.manager', string="Project")
    start_date = fields.Date(string="Start Date")
    end_date = fields.Date(string="End Date")
    description = fields.Text(string="Description")


