from odoo import api, fields, models

class SprintManager(models.Model):
    _name = 'sprint.manager'
    _description = 'Sprint management'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    project_id = fields.Many2one('project.manager', string="Project")
    start_date = fields.Date(string="Start Date")
    end_date = fields.Date(string="End Date")
    goal = fields.Text(string="Goal")

